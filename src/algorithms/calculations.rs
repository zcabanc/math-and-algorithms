//! Contains some routine calculation works.

/// Quick integer square root.
pub fn isqrt(num: u64) -> u64 {
  match num {
    0 => {
      return 0;
    }
    1 => {
      return 1;
    }
    2 => {
      return 1;
    }
    _ => {}
  }
  let mut current = num / 2;
  loop {
    if current * current <= num && (current + 1) * (current + 1) >= num {
      return current;
    }
    current = (current + num / current) / 2;
  }
}

#[test]
fn test_isqrt() {
	for num in 0..=1000u64 {
		let res = isqrt(num);
		let res_2 = res * res;
		assert!(res_2 <= num && num <= (res + 1) * (res + 1));
	}
}

/// Calculate 1 * 2 * ... * i
pub fn factorial(i: usize) -> usize {
  if i == 0 || i == 1 {
    return 1;
  }
  i.checked_mul(factorial(i - 1)).expect("Factorial overflow")
}
