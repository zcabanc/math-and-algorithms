//! Contain miscellaneous algorithms used in this project
//!
//! Might or might not be related to math.

pub mod array;
pub mod number_theory;
pub mod calculations;
