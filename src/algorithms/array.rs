//! Array manipulations - counting inversions, etc.

/// Merge two sorted array into one sorted array.
pub fn merge<E: Ord + Clone>(a: &[E], b: &[E]) -> Vec<E> {
	if a.len() == 0 {
		return b.to_owned();
	}
	if b.len() == 0 {
		return a.to_owned();
	}
	let mut result = Vec::with_capacity(a.len() + b.len());
	let mut a_ptr = 0usize;
	let mut b_ptr = 0usize;
	loop {
		if a_ptr >= a.len() {
			result.extend_from_slice(&b[b_ptr..]);
			break;
		} else if b_ptr >= b.len() {
			result.extend_from_slice(&a[a_ptr..]);
			break;
		} else {
			if a[a_ptr] <= b[b_ptr] {
				result.push(a[a_ptr].clone());
				a_ptr += 1;
			} else {
				result.push(b[b_ptr].clone());
				b_ptr += 1;
			}
		}
	}
	result
}

#[test]
fn test_merge() {
	assert_eq!(merge(&[], &[2]), &[2]);
	assert_eq!(merge(&[1], &[]), &[1]);
	assert_eq!(merge(&[1], &[2]), &[1,2]);
	assert_eq!(merge(&[2], &[1]), &[1,2]);
	assert_eq!(merge(&[1,3,5], &[2,4,6,8]), &[1,2,3,4,5,6,8]);
}

/// Returns the number of inversions in an array.
///
/// An inversion is a pair of indices (i, j) such that i < j and `array[i]` > `array[j]`.
pub fn inversions<E: Ord + Clone>(array: &[E]) -> usize {
	fn sort_and_count<E: Ord + Clone>(array: &mut [E]) -> usize {
		if array.len() <= 1 {
			return 0;
		}
		let (left, right) = array.split_at_mut(array.len() / 2);
		let (l_count, r_count) = (sort_and_count(left), sort_and_count(right));
		let mut cross_count = 0usize;
		let mut l = 0usize;
		let mut r = 0usize;
		loop {
			// Invariant: everything in right[0..r] is less than left[l].
			if r < right.len() && right[r] < left[l] {
				r += 1;
			} else {
				cross_count += r;
				l += 1; // Invariant maintained since if everything in right[0..r] is
								// less than left[l] and left[l] <= left[l+1], everything in right[0..r]
								// is less than left[l+1].
				if l == left.len() {
					break;
				}
			}
		}

		let merged = merge(left, right);
		assert_eq!(merged.len(), array.len());
		let mut i = 0usize;
		for val in merged.into_iter() {
			array[i] = val;
			i += 1;
		}
		l_count + r_count + cross_count
	}

	sort_and_count(&mut Vec::from(array))
}

#[test]
fn test_inversions() {
	assert_eq!(inversions(&Vec::<i32>::new()), 0);
	assert_eq!(inversions(&vec![1]), 0);
	assert_eq!(inversions(&vec![1,2,3,4]), 0);
	assert_eq!(inversions(&vec![2,2,2,2]), 0);
	assert_eq!(inversions(&vec![2,3,2,3]), 1);
	assert_eq!(inversions(&vec![3,2,1]), 3);
	assert_eq!(inversions(&vec![4,3,2,1]), 3+2+1);
	assert_eq!(inversions(&vec![1,3,2]), 1);
	assert_eq!(inversions(&vec![2,3,1]), 2);
	assert_eq!(inversions(&vec![2,3,4,1]), 3);

	assert_eq!(inversions(&vec![2,3,3]), 0);
	assert_eq!(inversions(&vec![3,3,2]), 2);
	assert_eq!(inversions(&vec![1,1]), 0);
}
