//! Algorithms that specifically deals with factors, primes, etc.

mod gcd;
mod factors;
pub use gcd::*;
pub use factors::*;
