use std::iter::Iterator;
use crate::algorithms::calculations::isqrt;

/// See [`iter_factors`].
pub struct FactorIterator {
  num: u64,
  search_limit: u64,
  next_try: u64,
  cached_next: u64,
}

/// Returns an iterator of all the divisors of a number, including 1 and itself.
pub fn iter_factors(num: u64) -> FactorIterator {
  FactorIterator {
    num,
    search_limit: isqrt(num),
    next_try: 1,
    cached_next: 0,
  }
}

impl Iterator for FactorIterator {
  type Item = u64;

  fn next(&mut self) -> Option<u64> {
    if self.cached_next != 0 {
      let cc = self.cached_next;
      self.cached_next = 0;
      return Some(cc);
    }
    if self.next_try > self.search_limit {
      return None;
    }
    if self.num % self.next_try == 0 {
      let factor = self.next_try;
      self.next_try += 1;
      let other_factor = self.num / factor;
      if other_factor != factor {
        self.cached_next = other_factor;
      } else {
        self.cached_next = 0;
      }
      return Some(factor);
    } else {
      self.next_try += 1;
      return self.next();
    }
  }
}

/// Return the number of divisors of a number, including 1 and itself.
pub fn nb_factors(num: u64) -> u64 {
  let search_limit = isqrt(num);
  let mut nf: u64 = 0;
  for i in 1..=search_limit {
    if num % i == 0 {
      nf += 1;
      if i * i != num {
        nf += 1;
      }
    }
  }
  return nf;
}

#[test]
fn num_factors() {
	for num in 0..=1000u64 {
		let mut expect = 0u64;
		for i in 1..=num {
			if num % i == 0 {
				expect += 1;
			}
		}
		assert_eq!(nb_factors(num), expect);
	}

	assert_eq!(nb_factors(1), 1);
	assert_eq!(nb_factors(2), 2);
}

#[test]
fn factors_iter() {
	for num in 0..=1000u64 {
		let expect_factor_count = nb_factors(num);
		let mut got_factor_count = 0u64;
		for fac in iter_factors(num) {
			got_factor_count += 1;
			if got_factor_count > expect_factor_count {
				panic!("Too much factor returned");
			}
			assert_eq!(num % fac, 0);
		}
		assert_eq!(expect_factor_count, got_factor_count);
	}
}
