/// Hold the result of [`gcd`].
///
/// `x` and `y` are such that `x * a + y * b` will be equal to `gcd`, where `a` and `b` are the input to [`gcd`].
#[derive(Debug)]
pub struct GcdResult {
  pub gcd: i64,
  pub x: i64,
  pub y: i64,
}

/// Compute the greatest common divisor of two numbers using the extended
/// euclidean algorithm. Returns the gcd and `x` and `y` such that `x * a + y * b = gcd`.
pub fn gcd(a: i64, b: i64) -> GcdResult {
	if a == 0 {
		return GcdResult{gcd: b, x: 0, y: 1};
	} else if b == 0 {
		return GcdResult{gcd: a, x: 1, y: 0};
	}

	if a < 0 || b < 0 {
		let mut result = gcd(a.abs(), b.abs());
		if a < 0 {
			result.x *= -1;
		}
		if b < 0 {
			result.y *= -1;
		}
		return result;
	}
	let mut current_a;
	let mut current_b;
	let mut current_a_comp = (1i64, 0);
	let mut current_b_comp = (0, 1i64);
	let abswapped;
	if b <= a {
		current_a = a;
		current_b = b;
		abswapped = false;
	} else {
		current_a = b;
		current_b = a;
		abswapped = true;
	}
	loop {
		let q = current_a / current_b;
		let rem = current_a % current_b;
		if rem == 0 {
			break;
		}

		let rem_comp = (
			current_a_comp.0 - current_b_comp.0 * q,
			current_a_comp.1 - current_b_comp.1 * q
		);

		current_a = current_b;
		current_a_comp = current_b_comp;
		current_b = rem;
		current_b_comp = rem_comp;
	}
	if !abswapped {
		GcdResult{gcd: current_b, x: current_b_comp.0, y: current_b_comp.1}
	} else {
		GcdResult{gcd: current_b, x: current_b_comp.1, y: current_b_comp.0}
	}
}

#[test]
fn test() {
	fn t(a: i64, b: i64, expected_gcd: i64) {
		let r = gcd(a, b);
		assert_eq!(r.gcd, expected_gcd);
		assert_eq!(r.x*a + r.y*b, expected_gcd);
	}

	t(0, 0, 0);
	t(0, 1, 1);
	t(2, 0, 2);
	t(1, 1, 1);
	t(1, 7, 1);
	t(-1, 1, 1);
	t(1, -1, 1);
	t(-1, -1, 1);
	t(1, 2, 1);
	t(1, -2, 1);
	t(2, 1, 1);
	t(2, -1, 1);
	t(3, 5, 1);
	t(3, -5, 1);
	t(3, 6, 3);
	t(-3, 6, 3);
	t(10, 15, 5);
	t(2,128,2);
	t(31*5, 31*23, 31);
}
