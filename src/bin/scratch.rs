use math_and_algorithms::groups::permutations::{Sn, Permutation};

fn main() {
	for p in Sn::new(4).iter() {
		println!("{} {:>2}", p.0.iter().map(|x| format!("{}", (*x) + 1)).collect::<Vec<_>>().join(", "), p.sign());
	}
}
