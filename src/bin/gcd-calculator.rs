use math_and_algorithms::algorithms::number_theory::gcd;

fn main() {
  use clap::{App, Arg};
  use std::str::FromStr;
  let args = App::new("gcd-calculator")
    .arg(Arg::with_name("a").required(true).index(1).help("First number"))
    .arg(Arg::with_name("b").required(true).index(2).help("Second number"))
    .get_matches();
  let a = i64::from_str(args.value_of("a").unwrap()).unwrap();
  let b = i64::from_str(args.value_of("b").unwrap()).unwrap();
  let gcd_result = gcd(a, b);
  fn ss(n: i64) -> String {
    if n >= 0 {
      format!("+ {}", n)
    } else {
      format!("- {}", -n)
    }
  }
  println!("gcd = {} = {}*{} {}*{}", gcd_result.gcd, gcd_result.x, a, ss(gcd_result.y), b);
}
