//! Utilities for dealing with permutations.

use std::cmp::{Ord, Eq};
use std::iter::Iterator;
use super::Group;
use crate::algorithms::calculations::factorial;

/// Represent a permutation on E. In order words, a bijection from E to E.
pub trait Permutation<E: Ord + Eq> {
	/// Apply the permutation function to `e`.
	fn apply(&self, e: E) -> E;

	/// Return the [sign](https://en.wikipedia.org/wiki/Parity_of_a_permutation) of
	/// this permutation.
	fn sign(&self) -> i8;
}

/// A permutation represented with a list of [`usize`]s, where `i` is taken to
/// `list[i]`.
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ListedPermutation(pub Vec<usize>);

impl Permutation<usize> for ListedPermutation {
	fn apply(&self, e: usize) -> usize {
		if self.0.len() <= e {
			panic!("{} outside domain of this permutation.", e);
		}
		self.0[e]
	}

	fn sign(&self) -> i8 {
		let inversions = crate::algorithms::array::inversions(&self.0);
		if inversions % 2 == 1 {
			-1
		} else {
			1
		}
	}
}

impl ListedPermutation {
	/// Wrap a `Vec<usize>` into a ListedPermutation.
	///
	/// ## Return
	/// * `Some` if the list is a valid permutation. Namely:
	/// 	* The list does not contain duplicate element;
	///   * For each i in the list, 0 <= i < n, where n = list.len()
	/// * Otherwise, `None`.
	pub fn checked_from_list(list: Vec<usize>) -> Option<Self> {
		let len = list.len();
		if len == 0 {
			return Some(ListedPermutation(list));
		}
		let mut seen = vec![false; len];
		for i in list.iter().map(|x| *x) {
			if i >= len {
				return None;
			}
			if seen[i] {
				return None;
			}
			seen[i] = true;
		}
		return Some(ListedPermutation(list))
	}

	/// Return the permutation [0,...,n-1].
	pub fn identity(n: usize) -> Self {
		Self((0..n).into_iter().collect())
	}

	/// Return the permutation that is lexicographical one more then self.
	///
	/// For example:
	/// ```
	/// # use math_and_algorithms::groups::permutations::ListedPermutation;
	/// assert_eq!(ListedPermutation(vec![0,1,2]).into_next().unwrap().0, vec![0,2,1]);
	/// assert_eq!(ListedPermutation(vec![0,2,1]).into_next().unwrap().0, vec![1,0,2]);
	/// ```
	pub fn into_next(mut self) -> Option<Self> {
		if self.inplace_next() {
			Some(self)
		} else {
			None
		}
	}

	/// Same as [`ListedPermutation::into_next`], except modifies self rather then taking self.
	///
	/// ## Return
	/// * `true` if self has changed;
	/// * `false` if self is already the "maximum" permutation.
	pub fn inplace_next(&mut self) -> bool {
		let arr = &mut self.0;
		if arr.len() <= 1 || arr.is_sorted_by(|a, b| Some(b.cmp(a))) {
			false
		} else {
			// 1 2 4 6 5 3

			let mut r = arr.len() - 1;
			while arr[r - 1] > arr[r] {
				r -= 1;
			}
			// 1 2 4 6 5 3
			//       ^ r

			let mut i = arr.len() - 1;
			while arr[i] < arr[r - 1] {
				i -= 1;
			}

			arr.swap(r-1, i);
			// 1 2 5 6 4 3
			//       ^ r
			arr[r..].reverse();

			true
		}
	}

	pub fn n(&self) -> usize {
		self.0.len()
	}
}

/// Group of permutations over `{x \in Z | 0 <= x < n}`.
pub struct Sn {
	n: usize
}

impl Sn {
	pub fn new(n: usize) -> Self {
		Sn{n}
	}

	/// Returns an iterator over all possible permutations in this group.
	///
	/// ## Example
	/// ```
	/// # use math_and_algorithms::groups::permutations::Sn;
	/// assert_eq!(Sn::new(3).iter().map(|x| x.0).collect::<Vec<Vec<usize>>>(), vec![
	/// 	vec![0,1,2],
	/// 	vec![0,2,1],
	/// 	vec![1,0,2],
	/// 	vec![1,2,0],
	/// 	vec![2,0,1],
	/// 	vec![2,1,0]
	/// ]);
	/// ```
	pub fn iter(&self) -> SnIterator {
		SnIterator{current: Some(self.id())}
	}
}

impl Group<ListedPermutation> for Sn {
	fn id(&self) -> ListedPermutation {
		ListedPermutation::identity(self.n)
	}
	fn mul(&self, a: ListedPermutation, b: ListedPermutation) -> ListedPermutation {
		debug_assert!(a.n() == self.n);
		debug_assert!(b.n() == self.n);
		ListedPermutation::checked_from_list((0..self.n).into_iter().map(|i| a.apply(b.apply(i))).collect()).expect("Invalid product?")
	}
	fn inverse(&self, a: ListedPermutation) -> ListedPermutation {
		debug_assert!(a.n() == self.n);
		let mut list = vec![0usize; self.n];
		for i in 0..self.n {
			let ai = a.apply(i);
			list[ai] = i;
		}
		ListedPermutation::checked_from_list(list).expect("Invalid inverse?")
	}
	fn order(&self) -> Option<usize> {
		Some(factorial(self.n))
	}
}

/// See [`Sn::iter`].
pub struct SnIterator {
	current: Option<ListedPermutation>
}

impl Iterator for SnIterator {
	type Item = ListedPermutation;

	fn next(&mut self) -> Option<Self::Item> {
		if let Some(ref mut current) = self.current {
			let current_clone = current.clone();
			if !current.inplace_next() {
				self.current = None;
			}
			Some(current_clone)
		} else {
			None
		}
	}
}

#[test]
fn test_listed_perm() {
	assert_eq!(ListedPermutation::checked_from_list(vec![0,0,1]), None);
	assert_eq!(ListedPermutation::checked_from_list(vec![10]), None);
	assert_eq!(ListedPermutation::checked_from_list(vec![0,1]), Some(ListedPermutation(vec![0,1])));
}

#[test]
fn test_next() {
	assert_eq!(ListedPermutation::checked_from_list(vec![]).unwrap().into_next(), None);
	assert_eq!(ListedPermutation::checked_from_list(vec![0]).unwrap().into_next(), None);
	assert_eq!(ListedPermutation::checked_from_list(vec![0,1]).unwrap().into_next(), Some(ListedPermutation(vec![1,0])));
	assert_eq!(ListedPermutation::checked_from_list(vec![1,0]).unwrap().into_next(), None);
}

#[test]
fn test_inverse() {
	assert_eq!(Sn::new(4).inverse(ListedPermutation::checked_from_list(vec![2,0,1,3]).unwrap()).0, vec![1,2,0,3])
}

#[test]
fn test_mul() {
	let s4 = Sn::new(4);
	assert_eq!(s4.mul(ListedPermutation(vec![1,0,2,3]), ListedPermutation(vec![0,1,3,2])).0, vec![1,0,3,2]);
}
