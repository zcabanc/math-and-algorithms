//! Contain useful mathematical constructs that are groups, such as
//! permutations, integer modular arithmetic, etc.

mod group;
pub use group::*;
pub mod permutations;
pub mod modular_mul;
