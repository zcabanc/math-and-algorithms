//! The Z/Zn* group.

use crate::algorithms::number_theory::gcd;
use super::Group;

pub struct ModMulGroup {
	pub m: i64
}

impl ModMulGroup {
	pub fn new(m: i64) -> Self {
		if m <= 0 {
			panic!("Expected positive integer.");
		}
		Self{m}
	}

	pub fn to_element(&self, n: i64) -> Option<i64> {
		let modded = n.rem_euclid(self.m);
		if gcd(modded, self.m).gcd == 1 {
			Some(modded)
		} else {
			None
		}
	}
}

impl Group<i64> for ModMulGroup {
	fn id(&self) -> i64 {
		1
	}
	fn mul(&self, a: i64, b: i64) -> i64 {
		debug_assert!(self.to_element(a).is_some() && self.to_element(b).is_some());
		self.to_element(a*b).expect("Unreachable: product does not exist in group?")
	}
	fn inverse(&self, a: i64) -> i64 {
		debug_assert!(self.to_element(a).is_some());
		let g = gcd(a, self.m);
		if g.gcd != 1 {
			unreachable!("Inverse not exist in group?");
		}
		self.to_element(g.x).expect("Unreachable: inverse not mapable to group?")
	}
	fn order(&self) -> Option<usize> {
		let mut result = 0usize;
		for i in 1..=self.m {
			if gcd(i, self.m).gcd == 1 {
				result += 1;
			}
		}
		Some(result)
	}
}

#[test]
fn test_to_element() {
	assert_eq!(ModMulGroup::new(25).to_element(20), None);
	assert_eq!(ModMulGroup::new(25).to_element(21), Some(21));
	assert_eq!(ModMulGroup::new(25).to_element(21+25), Some(21));
}

#[test]
fn test_mul() {
	let zz7 = ModMulGroup::new(7);
	assert_eq!(zz7.mul(1, 2), 2);
	assert_eq!(zz7.mul(2, 6), 5);
	assert_eq!(zz7.mul(1, 1), 1);
	assert_eq!(zz7.to_element(-1), Some(6));
	assert_eq!(zz7.mul(1, -1), 6);
}

#[test]
fn test_inverse() {
	fn t(a: i64, m: i64) {
		let zzm = ModMulGroup::new(m);
		let inv = zzm.inverse(a);
		if zzm.mul(a, inv) != 1 {
			panic!("{} * {} != 1 mod {}", a, inv, m);
		}
		assert_eq!(zzm.mul(zzm.inverse(a), a), 1);
	}

	assert_eq!(ModMulGroup::new(2).inverse(1), 1);
	t(1,2);
	t(3,5);
	t(22,23);
}

#[test]
fn test_etf() {
	fn etf(m: i64) -> usize {
		ModMulGroup::new(m).order().unwrap()
	}
	assert_eq!(etf(1), 1);
	assert_eq!(etf(2), 1);
	assert_eq!(etf(9), 6);
}
