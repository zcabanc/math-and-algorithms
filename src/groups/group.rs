/// Represent groups.
pub trait Group<E: Clone + Eq + std::fmt::Debug> {
	/// Return the element 1 of this group such that for every element e in this group, mul(e, 1) = mul(1, e) = e
	fn id(&self) -> E;

	/// Return the group law applied on a and b, written as a * b. Not necessarily commutative.
	fn mul(&self, a: E, b: E) -> E;

	/// Return the unique element a^-1 such that a^-1 * a = a * a^-1 = id()
	fn inverse(&self, a: E) -> E;

	/// For a finite group, return the number of elements in the group.
	fn order(&self) -> Option<usize>;

	/// Compute a * a * ... * a (`exp` `a`s)
	fn power(&self, a: E, exp: usize) -> E {
		let mut accum = self.id();
		let exp = match self.order() {
			None => exp,
			Some(ord) => exp % ord
		};
		if exp == 0 {
			return accum;
		}
		accum = a.clone();
		let mut current_exp = 1;
		loop {
			if current_exp*2 <= exp {
				current_exp *= 2;
				accum = self.mul(accum.clone(), accum.clone());
			} else if current_exp < exp {
				current_exp += 1;
				accum = self.mul(accum, a.clone());
			} else {
				break;
			}
		}
		accum
	}
}
