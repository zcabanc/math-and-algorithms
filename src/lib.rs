//! Tingmao's algorithm and discrete math code

#![feature(trait_alias)]
#![feature(is_sorted)]

pub mod algorithms;
pub mod groups;
pub mod linear_algebra;
