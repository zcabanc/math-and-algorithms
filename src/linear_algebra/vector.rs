use super::Field;

pub trait Vector<F: Field>: Copy + Clone + std::fmt::Debug {
	fn add_id() -> Self;
	fn add(&self, b: Self) -> Self;
	fn scale(&self, s: F) -> Self;
	fn neg(&self) -> Self;

	fn sub(&self, b: Self) -> Self {
		self.add(b.neg())
	}
}
