//! Raw matrix operations.

use super::Field;

#[derive(Debug)]
pub struct Matrix<F: Field> {
	rows: usize,
	columns: usize,
	content: Vec<F>,
}

impl<F: Field> Matrix<F> {
	pub fn zeroed(rows: usize, columns: usize) -> Self {
		Self{
			rows, columns, content: vec![F::add_id(); rows*columns]
		}
	}

	pub fn identity(n: usize) -> Self {
		let mut m = Self::zeroed(n, n);
		for i in 0..n {
			m.put_entry(i, i, F::mul_id());
		}
		m
	}

	pub fn from_row_col_list(stride: usize, data: &[F]) -> Self {
		if data.len() % stride != 0 {
			panic!("length of list (={}) not divisiable by stride (={})", data.len(), stride);
		}
		Self{
			rows: data.len() / stride,
			columns: stride,
			content: data.to_owned()
		}
	}

	fn row_and_col_to_idx(&self, row: usize, col: usize) -> usize {
		assert!(row < self.rows);
		assert!(col < self.columns);
		row*self.columns + col
	}

	pub fn entry(&self, row: usize, col: usize) -> &F {
		&self.content[self.row_and_col_to_idx(row, col)]
	}

	pub fn put_entry(&mut self, row: usize, col: usize, value: F) {
		*self.entry_mut(row, col) = value;
	}

	pub fn entry_mut(&mut self, row: usize, col: usize) -> &mut F {
		let i = self.row_and_col_to_idx(row, col);
		&mut self.content[i]
	}

	pub fn is_square(&self) -> bool {
		self.rows == self.columns
	}

	pub fn det(&self) -> F {
		if !self.is_square() {
			panic!("Can't compute determinant of a non-square matrix.");
		}
		if self.rows == 0 {
			panic!("Can't compute determinant of the empty matrix.");
		}
		let mut sum = F::add_id();
		use crate::groups::permutations::{Sn, Permutation};
		for p in Sn::new(self.rows).iter() {
			let mut accum_mul = F::mul_id();
			for i in 0..self.rows {
				accum_mul = accum_mul.mul(self.entry(i, p.apply(i)).clone());
			}
			if p.sign() == -1 {
				accum_mul = accum_mul.neg();
			}
			sum = sum.add(accum_mul);
		}
		sum
	}

	/// Return (rows, columns)
	pub fn dimension(&self) -> (usize, usize) {
		(self.rows, self.columns)
	}
}

#[test]
fn test_matrix() {
	for n in 1..5 {
		let id: Matrix<i32> = Matrix::identity(n);
		assert!(id.is_square());
		if n == 2 {
			assert_eq!(&id.content, &vec![1,0,0,1]);
		}
		assert_eq!(id.det(), 1);
		let zero: Matrix<i32> = Matrix::zeroed(n, n);
		assert_eq!(zero.det(), 0);
	}
}

#[test]
fn test_det() {
	assert_eq!(Matrix::from_row_col_list(3, &vec![2,1,7,4,-1,2,1,3,5]).det(), 51);
}

use std::fmt;

impl<F: Field + fmt::Display> fmt::Display for Matrix<F> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		let max_width = self.content.iter().map(|x| format!("{}", x).len()).max().unwrap();
		write!(f, "[ ")?;
		for y in 0..self.rows {
			for x in 0..self.columns {
				if x != 0 {
					write!(f, ", ")?;
				}
				write!(f, "{:>space$}", &self.entry(y, x), space = max_width)?;
			}
			if y + 1 < self.rows {
				write!(f, ";\n  ")?;
			}
		}
		write!(f, "]")
	}
}
