/// A number field, as defined in linear algebra.
pub trait Field: Clone + PartialEq + std::fmt::Debug {
	fn add_id() -> Self;
	fn mul_id() -> Self;

	fn add(self, b: Self) -> Self;
	fn mul(self, b: Self) -> Self;
	fn neg(self) -> Self;
	fn rec(self) -> Self;

	fn sub(self, b: Self) -> Self {
		self.add(b.neg())
	}
	fn div(self, b: Self) -> Self {
		self.mul(b.rec())
	}
}

macro_rules! impl_field {
	($to_type:ident) => {
		impl Field for $to_type {
			fn add_id() -> Self {
				0 as Self
			}
			fn mul_id() -> Self {
				1 as Self
			}
			fn add(self, b: Self) -> Self {
				self + b
			}
			fn mul(self, b: Self) -> Self {
				self * b
			}
			fn neg(self) -> Self {
				-self
			}
			fn rec(self) -> Self {
				Self::mul_id() / self
			}
			fn sub(self, b: Self) -> Self {
				self - b
			}
			fn div(self, b: Self) -> Self {
				self / b
			}
		}
	};
}

impl_field!(i8);
impl_field!(i16);
impl_field!(i32);
impl_field!(i64);
impl_field!(f32);
impl_field!(f64);
